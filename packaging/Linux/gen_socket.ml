let pp_service ppf = function
  | `Console -> Fmt.pf ppf "console"
  | `Vmmd -> Fmt.pf ppf "daemon"
  | `Stats -> Fmt.pf ppf "stats"
  | `Log -> Fmt.pf ppf "log"

let unprivileged_services = [ `Console; `Log; `Stats ]
let privileged_services = [ `Vmmd ]

let unprivileged_format : _ format= {|[Unit]
Description=Albatross %s socket
PartOf=%s

[Socket]
ListenStream=%s
SocketUser=albatross
SocketMode=0600
Accept=no

[Install]
WantedBy=sockets.target
|}

let privileged_format : _ format = {|[Unit]
Description=Albatross %s socket
PartOf=%s

[Socket]
ListenStream=%s
SocketMode=0600
Accept=no

[Install]
WantedBy=sockets.target
|}

let write_socket privileged service =
  let out_file = Fmt.str "albatross_%a.socket" pp_service service in
  let systemd_service = Fmt.str "albatross_%a.service" pp_service service in
  let service_name = Fmt.str "%a" pp_service service in
  let socket_path = Vmm_core.socket_path (service :> Vmm_core.service) in
  let oc = open_out out_file in
  let ppf = Format.formatter_of_out_channel oc in
  Fmt.pf ppf (if privileged then privileged_format else unprivileged_format)
    service_name systemd_service socket_path

let () =
  let tmpdir = Fpath.(v "%t" / "albatross") in
  let () = Vmm_core.set_tmpdir tmpdir in
  List.iter (write_socket false) unprivileged_services;
  List.iter (write_socket true) privileged_services
